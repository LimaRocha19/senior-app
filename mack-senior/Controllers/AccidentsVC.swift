//
//  AccidentsVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 30/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit

class AccidentsVC: UITableViewController {

    var patient: Patient!

    var accidents: [Log] = []

    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)

    override func viewDidLoad() {
        super.viewDidLoad()

        self.spinner.hidesWhenStopped = true
        let item = UIBarButtonItem(customView: self.spinner)
        self.navigationItem.rightBarButtonItems = [item]
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.spinner.startAnimating()
        Service.emergency(_id: self.patient._id) { (status) in
            self.spinner.stopAnimating()
            switch status {
            case .success(let logs):
                self.accidents = logs
                self.tableView.reloadData()
            case .failure(let error):
                let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.accidents.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accident", for: indexPath)

        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"

        cell.textLabel?.text = formatter.string(from: self.accidents[indexPath.row].created)

        return cell
    }
}
