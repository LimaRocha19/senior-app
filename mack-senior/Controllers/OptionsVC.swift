//
//  OptionsVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 30/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit

class OptionsVC: UITableViewController {

    var patient: Patient!

    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)

    override func viewDidLoad() {
        super.viewDidLoad()

        spinner.hidesWhenStopped = true
        let item = UIBarButtonItem(customView: self.spinner)
        self.navigationItem.rightBarButtonItems = [item]

        self.navigationItem.title = patient.name
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Service.latest(_id: self.patient._id) { (status) in
            switch status {
            case .success(let interval):
                if (Date().timeIntervalSince1970 - interval > 1 * 60) {
                    let controller = UIAlertController(title: "Atenção!", message: "Este paciente teve dados coletados pela última vez há mais de 30 minutos! Sugerimos que você cheque se tudo está bem com ele.", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Certo!", style: .cancel, handler: nil)
                    controller.addAction(ok)
                    self.present(controller, animated: true, completion: nil)
                }
            case .failure(_):
                print(#function, "error")
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bpm" {
            guard let controller = segue.destination as? BPMVC else {
                return
            }
            controller.patient = self.patient
        }
        if segue.identifier == "accidents" {
            guard let controller = segue.destination as? AccidentsVC else {
                return
            }
            controller.patient = self.patient
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                print(#function, "Batimentos")
                self.performSegue(withIdentifier: "bpm", sender: self)
            default:
                print(#function, "Acidentes")
                self.performSegue(withIdentifier: "accidents", sender: self)
            }
        default:
            print(#function, "Excluindo Paciente")

            let controller = UIAlertController(title: "Excluindo Paciente", message: "Esta ação não poderá ser desfeita.", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let proceed = UIAlertAction(title: "Excluir", style: .destructive) { (action) in
                switch Service.user.type {
                case .care:
                    self.spinner.startAnimating()
                    Service.delete(_id: self.patient._id) { (status) in
                        self.spinner.stopAnimating()
                        switch status {
                        case .success(let msg):

                            Service.user.remove(_id: self.patient._id)

                            let controller = UIAlertController(title: "Eba :D", message: msg, preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            controller.addAction(ok)
                            self.present(controller, animated: true, completion: nil)
                        case .failure(let error):
                            let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                            controller.addAction(ok)
                            self.present(controller, animated: true, completion: nil)
                        }
                    }
                case .family:
                    self.spinner.startAnimating()
                    Service.delete_family(_id: self.patient._id) { (status) in
                        self.spinner.stopAnimating()
                        switch status {
                        case .success(let msg):

                            Service.user.remove(_id: self.patient._id)

                            let controller = UIAlertController(title: "Eba :D", message: msg, preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            controller.addAction(ok)
                            self.present(controller, animated: true, completion: nil)
                        case .failure(let error):
                            let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                            controller.addAction(ok)
                            self.present(controller, animated: true, completion: nil)
                        }
                    }
                }
            }
            controller.addAction(cancel)
            controller.addAction(proceed)
            self.present(controller, animated: true, completion: nil)
        }
    }
}
