//
//  BPMVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 30/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit
import InputMask

class BPMVC: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
//    @IBOutlet weak var beginTF: UITextField!
//    @IBOutlet weak var endTF: UITextField!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var date: UIDatePicker!

    let intervals: [Double] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    
    //    @IBOutlet weak var listener: MaskedTextFieldDelegate!

    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)

    var patient: Patient!

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.beginTF.delegate = self
//        self.endTF.delegate = self

        self.picker.delegate = self
        self.picker.dataSource = self

        self.date.maximumDate = Date()

//        self.listener.primaryMaskFormat = "[00]/[00]/[0000]"

        self.minLabel.text = ""
        self.maxLabel.text = ""

        self.spinner.hidesWhenStopped = true
        let item = UIBarButtonItem(customView: self.spinner)
        self.navigationItem.rightBarButtonItems = [item]
    }

//    open func textField(
//        _ textField: UITextField,
//        didFillMandatoryCharacters complete: Bool,
//        didExtractValue value: String
//        ) {
//        print(value)
//    }

//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
////        textField.resignFirstResponder()
////        self.beginTF.resignFirstResponder()
////        self.endTF.resignFirstResponder()
//        return true
//    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.intervals.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(self.intervals[row])"
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 2 {

            let beginDate = self.date.date - self.intervals[self.picker.selectedRow(inComponent: 0)] * 60 / 2
            let endDate = self.date.date + self.intervals[self.picker.selectedRow(inComponent: 0)] * 60 / 2

            let begin = beginDate.timeIntervalSince1970
            let end = endDate.timeIntervalSince1970

            self.spinner.startAnimating()
            Service.bpm(_id: self.patient._id, start: begin, end: end) { (status) in
                self.spinner.stopAnimating()
                switch status {
                case .success(let log):
                    self.minLabel.text = "\((log.data["min"] as? Double) ?? 0)"
                    self.maxLabel.text = "\((log.data["max"] as? Double) ?? 0)"

                    if ((log.data["min"] as? Double) ?? 0) == 1000.0 || ((log.data["max"] as? Double) ?? 0) == 0.0 {
                        let controller = UIAlertController(title: "Erro!", message: "Não há amostras para este período.", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Certo!", style: .cancel, handler: nil)
                        controller.addAction(ok)
                        self.present(controller, animated: true, completion: nil)
                        return
                    }

                    if ((log.data["min"] as? Double) ?? 0) <= 40 || ((log.data["max"] as? Double) ?? 0) >= 200 {
                        let controller = UIAlertController(title: "Alerta!", message: "Foram observados valores anormais de batimentos nesta amostra. Verifique o estado do paciente.", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Certo!", style: .cancel, handler: nil)
                        controller.addAction(ok)
                        self.present(controller, animated: true, completion: nil)
                    }

                case .failure(let error):
                    let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    controller.addAction(ok)
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
}
