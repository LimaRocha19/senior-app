//
//  PatientsVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 16/09/2018.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit

class PatientsVC: UITableViewController {

    fileprivate var nameTF: UITextField!
    fileprivate var birthTF: UITextField!

    fileprivate var selected: Patient!

    var patients: [Patient] = []

    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .white)

    override func viewDidLoad() {
        super.viewDidLoad()

        let item = UIBarButtonItem(title: "Sair", style: .done, target: self, action: #selector(PatientsVC.logout))
        //        let item = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(PatientsVC.logout))
        self.navigationItem.leftBarButtonItems = [item]

        self.spinner.hidesWhenStopped = true
        let spin = UIBarButtonItem(customView: self.spinner)
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(PatientsVC.new))
        self.navigationItem.rightBarButtonItems = [add,spin]
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.spinner.startAnimating()
        Service.patients(cached: false) { (status) in
            self.spinner.stopAnimating()
            switch status {
            case .success(let patients):
                self.patients = []
                Service.user.erase()
                Service.user.add(patients: patients)
                self.patients = Service.user.patients
                self.tableView.reloadData()
            case .failure(let error):
                let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "patient" {
            guard let controller = segue.destination as? OptionsVC else {
                return
            }
            controller.patient = selected
        }
    }

    @objc func logout() {
        Service.logout()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    @objc func new() {
        switch Service.user.type {
        case .family:
            self.performSegue(withIdentifier: "scan", sender: self)
        case .care:
            let controller = UIAlertController(title: "Novo Paciente", message: "Preencha os Campos Abaixo", preferredStyle: .alert)
            controller.addTextField { (tf) in
                tf.placeholder = "Nome do Paciente"
                self.nameTF = tf
            }
            controller.addTextField { (tf) in
                tf.placeholder = "Nascimento DD/MM/AAAA"
                self.birthTF = tf
            }
            let add = UIAlertAction(title: "Adicionar", style: .default) { (action) in
                if self.nameTF.text! == "" || self.birthTF.text! == "" {
                    let controller = UIAlertController(title: "Erro :(", message: "Preencha todos os campos!", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    controller.addAction(ok)
                    self.present(controller, animated: true, completion: nil)
                    return
                }

                self.spinner.startAnimating()
                Service.add(params: ["name" : self.nameTF.text!, "birth" : self.birthTF.text!], completion: { (status) in
                    self.spinner.stopAnimating()
                    switch status {
                    case .success(let patient):
                        Service.user.add(patient: patient)
                        self.patients.append(patient)
                        self.tableView.reloadData()
                    case .failure(let error):
                        let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        controller.addAction(ok)
                        self.present(controller, animated: true, completion: nil)
                    }
                })
            }
            let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            controller.addAction(add)
            controller.addAction(cancel)

            self.present(controller, animated: true, completion: nil)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "patient", for: indexPath)

        cell.textLabel?.text = self.patients[indexPath.row].name

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selected = self.patients[indexPath.row]
        self.performSegue(withIdentifier: "patient", sender: nil)
    }
}
