//
//  LoginVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 16/09/2018.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.emailTF.delegate = self
        self.passwordTF.delegate = self
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//
//        if Service.isLogged {
//            self.spinner.startAnimating()
//            Service.profile { (status) in
//                self.spinner.stopAnimating()
//                switch status {
//                case .success(let user):
//                    print(#function, user)
//                    Service.user = user
//                    self.performSegue(withIdentifier: "login", sender: nil)
//                case .failure(let error):
//                    let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
//                    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
//                    controller.addAction(ok)
//                    self.present(controller, animated: true, completion: nil)
//                }
//            }
//        }
//    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signup" {
            guard let controller = segue.destination as? SignupVC else {
                return
            }
            controller.loginVC = self
        }
    }

    @IBAction func signin(_ sender: Any) {

        if self.emailTF.text! == "" || self.passwordTF.text! == "" {
            let controller = UIAlertController(title: "Erro :(", message: "Preencha todos os campos!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            controller.addAction(ok)
            self.present(controller, animated: true, completion: nil)
            return
        }

        self.spinner.startAnimating()
        Service.signin(params: ["email" : self.emailTF.text!,
                                "password" : self.passwordTF.text!]) { (status) in
            self.spinner.stopAnimating()
            switch status {
            case .success(let user):
                print(#function, user)
                Service.user = user
                self.performSegue(withIdentifier: "login", sender: nil)
            case .failure(let error):
                let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }

    }
    
    @IBAction func recover(_ sender: Any) {

        let controller = UIAlertController(title: "Recuperar Senha", message: "Enviaremos um e-mail para o endereço preenchido na tela de login com maiores instruções.", preferredStyle: .actionSheet)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            print(#function, "Recover password")

            self.spinner.startAnimating()
            Service.reset_password(email: self.emailTF.text!) { (status) in
                self.spinner.stopAnimating()
                switch status {
                case .success(let msg):
                    let controller = UIAlertController(title: "Eba :D", message: msg, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    controller.addAction(ok)
                    self.present(controller, animated: true, completion: nil)
                case .failure(let error):
                    let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    controller.addAction(ok)
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)

        controller.addAction(ok)
        controller.addAction(cancel)

        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func signup(_ sender: Any) {
        self.performSegue(withIdentifier: "signup", sender: self)
    }
}

extension LoginVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTF:
            self.passwordTF.becomeFirstResponder()
            return false
        case self.passwordTF:
            textField.resignFirstResponder()
            return false
        default:
            textField.resignFirstResponder()
            return true
        }
    }
}
