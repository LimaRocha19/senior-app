//
//  SignupVC.swift
//  mack-senior
//
//  Created by Isaías Lima on 16/09/2018.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var lastTF: UITextField!
    @IBOutlet weak var cellTF: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var loginVC: LoginVC!

    var type = "care"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.email.delegate = self
        self.passwordTF.delegate = self
        self.nameTF.delegate = self
        self.lastTF.delegate = self
        self.cellTF.delegate = self
    }

    @IBAction func segment(_ sender: Any) {
//        print(#function, sender)
        guard let segment = sender as? UISegmentedControl else {
            return
        }
        print(#function, segment.selectedSegmentIndex)
        switch segment.selectedSegmentIndex {
        case 0:
            self.type = "care"
        case 1:
            self.type = "family"
        default:
            self.type = "family"
        }
    }
    
    @IBAction func signup(_ sender: Any) {
        print(#function, "Signing Up")

        if self.email.text! == "" || self.passwordTF.text! == "" || self.nameTF.text! == "" || self.lastTF.text! == "" || self.cellTF.text! == "" {
            let controller = UIAlertController(title: "Erro :(", message: "Preencha todos os campos!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            controller.addAction(ok)
            self.present(controller, animated: true, completion: nil)
            return
        }

        let cell = self.cellTF.text!.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "")

        self.spinner.startAnimating()
        Service.signup(params: ["email" : self.email.text!,
                                "password" : self.passwordTF.text!,
                                "name" : self.nameTF.text!,
                                "last" : self.lastTF.text!,
                                "telephone" : cell,
                                "class" : self.type]) { (status) in
            self.spinner.stopAnimating()
            switch status {
            case .success(let user):
                print(#function, user)
                Service.user = user
                self.dismiss(animated: true, completion: {
                    self.loginVC.performSegue(withIdentifier: "login", sender: self.loginVC)
                })
            case .failure(let error):
                let controller = UIAlertController(title: "Erro :(", message: error.localizedDescription, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                controller.addAction(ok)
                self.present(controller, animated: true, completion: nil)
            }
        }
    }

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension SignupVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        switch textField {
        case self.email:
            self.passwordTF.becomeFirstResponder()
            return false
        case self.passwordTF:
            self.nameTF.becomeFirstResponder()
            return false
        case self.nameTF:
            self.lastTF.becomeFirstResponder()
            return false
        case self.lastTF:
            self.cellTF.becomeFirstResponder()
            return false
        case self.cellTF:
            textField.resignFirstResponder()
            return true
        default:
            textField.resignFirstResponder()
            return true
        }
    }
}
