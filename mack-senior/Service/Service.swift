//
//  Service.swift
//  mack-senior
//
//  Created by Isaías Lima on 29/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import Alamofire
import JASON

public enum RequestStatus<T> {
    case success(T)
    case failure(Error)
}

fileprivate struct API {

    typealias DyURL = (String) -> String

    private init() {}

    static let base = "http://ec2-18-231-64-83.sa-east-1.compute.amazonaws.com:5000/"
    static let user: DyURL = {
        return base + "user/\($0)/"
    }
    static let patient: DyURL = {
        return base + "patient/\($0)/"
    }
    static let log: DyURL = {
        return base + "log/\($0)/"
    }
}

class Service {

    static let isMockup: Bool = false

    static var user = User.mockup
    static var cache: [String : Any] = [:]

    static var cookie: HTTPCookie? {
        get {
            let defaults = UserDefaults.standard
            guard let properties = defaults.object(forKey: "kCookie") as? [HTTPCookiePropertyKey : Any] else {
                return nil
            }
            let cookie = HTTPCookie(properties: properties)
            return cookie
        } set(cke) {
            let defaults = UserDefaults.standard
            defaults.set(cke?.properties, forKey: "kCookie")
            defaults.synchronize()
        }
    }

    static var isLogged: Bool {
        guard let cookie = cookie else {
            return false
        }
        guard let expiration = cookie.expiresDate else {
            return false
        }
        if expiration.timeIntervalSince1970 < Date().timeIntervalSince1970 {
            return false
        } else {
            return true
        }
    }

    class func logout() {
        cookie = nil
    }

    // user routes ----------------------------------------------------------------------------------------------------


    class func signup(params: [String : String], completion: @escaping (RequestStatus<User>) -> Void) {

        if isMockup {
            user = User.mockup
            completion(.success(User.mockup))
            return
        }

        print(#function, API.user("signup"))

        let parameters: Parameters = params
        Alamofire.request(API.user("signup"), method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue
            if success {

                guard let resp = response.response
                    , let fields = resp.allHeaderFields as? [String : String]
                    , let url = resp.url
                    , let ckie = HTTPCookie.cookies(withResponseHeaderFields: fields, for: url).first else {
                        let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível estabelecer uma sessão com o servidor"]) as Error
                        completion(.failure(error))
                        return
                }
                cookie = ckie

                guard let usr = User(json: json["user"].json) else {
                    let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível processar os dados enviados pelo servidor. Contate os desenvolvedores."]) as Error
                    completion(.failure(error))
                    return
                }

                user = usr

                completion(.success(usr))

            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }

    }

    class func signin(params: [String : String], completion: @escaping (RequestStatus<User>) -> Void) {

        if isMockup {
            user = User.mockup
            completion(.success(User.mockup))
            return
        }

        print(#function, API.user("signin"))

        let parameters: Parameters = params
        Alamofire.request(API.user("signin"), method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue
            if success {

                guard let resp = response.response
                    , let fields = resp.allHeaderFields as? [String : String]
                    , let url = resp.url
                    , let ckie = HTTPCookie.cookies(withResponseHeaderFields: fields, for: url).first else {
                        let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível estabelecer uma sessão com o servidor"]) as Error
                        completion(.failure(error))
                        return
                }
                cookie = ckie

                guard let usr = User(json: json["user"].json) else {
                    let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível processar os dados enviados pelo servidor. Contate os desenvolvedores."]) as Error
                    completion(.failure(error))
                    return
                }

                user = usr

                completion(.success(usr))

            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }

    }

    class func profile(completion: @escaping (RequestStatus<User>) -> Void) {

        if isMockup {
            completion(.success(user))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        Alamofire.request(API.user("profile"), method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                guard let usr = User(json: json["user"].json) else {
                    let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível processar os dados enviados pelo servidor. Contate os desenvolvedores."]) as Error
                    completion(.failure(error))
                    return
                }

                user = usr

                completion(.success(usr))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func patients(cached: Bool = true, completion: @escaping (RequestStatus<[Patient]>) -> Void) {

        if isMockup {
//            user.add(patients: Patient.mockup)
            completion(.success(Patient.mockup))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        Alamofire.request(API.user("patients"), method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON { (response) in
            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                let patients = json["patients"].compactMap({ Patient(json: $0) })
                user.add(patients: patients)
                completion(.success(patients))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func reset_password(email: String, completion: @escaping (RequestStatus<String>) -> Void) {

        if isMockup {
            completion(.success("Verifique sua caixa de e-mail para conferir como alterar sua senha."))
            return
        }

        let parameters: Parameters = ["email" : email]
        Alamofire.request(API.user("forgot"), method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                completion(.success(json["message"].stringValue))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func update(telephone: String, completion: @escaping (RequestStatus<String>) -> Void) {

        if isMockup {
            user.telephone = telephone
            completion(.success("Telefone atualizado com sucesso."))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)
    }

    // patient routes -------------------------------------------------------------------------------------------------

    class func add(params: [String : String], completion: @escaping (RequestStatus<Patient>) -> Void) {

        if isMockup {
            guard let name = params["name"]
                , let birth = params["birth"] else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Parâmetros Faltando"])
                completion(.failure(error))
                return
            }
            let patient = Patient(name: name, birth: birth, _id: UUID().uuidString)
            user.add(patient: patient)
            completion(.success(patient))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = params
        Alamofire.request(API.patient("add"), method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {

                guard let patient = Patient(json: json["patient"].json) else {
                    let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível processar os dados enviados pelo servidor. Contate os desenvolvedores."]) as Error
                    completion(.failure(error))
                    return
                }

//                user.add(patient: patient)

                completion(.success(patient))

            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func delete(_id: String, completion: @escaping (RequestStatus<String>) -> Void) {

        if isMockup {
            user.remove(_id: _id)
            completion(.success("Paciente excluído com sucesso de nossa base de dados."))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["_id" : _id]
        Alamofire.request(API.patient("delete"), method: .delete, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                completion(.success(json["message"].stringValue))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }

        }
    }

//    class func update(params: [String : String], completion: @escaping (RequestStatus<String>) -> Void) {
//
//    }

    class func add_family(_id: String, completion: @escaping (RequestStatus<String>) -> Void) {

        if isMockup {
            completion(.success("Familiar adicionado!"))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["_id" : _id]
        Alamofire.request(API.patient("add_family"), method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                completion(.success(json["message"].stringValue))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func delete_family(_id: String, completion: @escaping (RequestStatus<String>) -> Void) {

        if isMockup {
            completion(.success("Familiar excluído!"))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["_id" : _id]
        Alamofire.request(API.patient("delete_family"), method: .delete, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in

            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                completion(.success(json["message"].stringValue))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }

        }
    }

    // log routes ------------------------------------------------------------------------------------------------------

    class func bpm(_id: String, start: TimeInterval, end: TimeInterval, completion: @escaping (RequestStatus<Log>) -> Void) {

        if isMockup {
            completion(.success(Log.bpm_mockup))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["patient" : _id,
                                      "start" : start * 1000,
                                      "end" : end * 1000]
        Alamofire.request(API.log("bpm"), method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                guard let min = json["min"].double
                    , let max = json["max"].double else {
                        let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Não foi possível processar os dados enviados pelo servidor. Contate os desenvolvedores."]) as Error
                        completion(.failure(error))
                        return
                }
                let log = Log(type: .bpm, created: Date(), data: ["min" : min,
                                                                  "max" : max])
                completion(.success(log))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func emergency(_id: String, completion: @escaping (RequestStatus<[Log]>) -> Void) {

        if isMockup {
            completion(.success(Log.emergency_mockup))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["patient" : _id]
        Alamofire.request(API.log("emergency"), method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                let logs = json["logs"].compactMap({ Log(emergencyJson: $0) })
                completion(.success(logs))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }

    class func latest(_id: String, completion: @escaping (RequestStatus<TimeInterval>) -> Void) {

        if isMockup {
            completion(.success(Date().timeIntervalSince1970))
            return
        }

        guard let cookie = cookie else {
            let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : "Esta sessão foi expirada, faça login novamente"]) as Error
            completion(.failure(error))
            return
        }
        HTTPCookieStorage.shared.setCookie(cookie)

        let parameters: Parameters = ["patient" : _id]
        Alamofire.request(API.log("latest"), method: .get, parameters: parameters, encoding: URLEncoding.default).responseJSON { (response) in
            guard let value = response.result.value else {
                let error = response.result.error!
                completion(.failure(error))
                return
            }

            let json = JSON(value)
            let success = json["success"].boolValue

            if success {
                let interval = (json["log"].json["created"].double ?? 0)/1000
                completion(.success(interval))
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: [NSLocalizedDescriptionKey : json["message"].stringValue]) as Error
                completion(.failure(error))
            }
        }
    }
}
