//
//  Log.swift
//  mack-senior
//
//  Created by Isaías Lima on 30/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit
import JASON

enum LogType: String {
    case bpm = "bpm"
    case location = "location"
    case emergency = "emergency"
}

class Log {

    var type: LogType
    var created: Date
    var data: [String : Any]

    static var bpm_mockup: Log {
        return Log(type: .bpm, created: Date(), data: ["min" : 60.0,
                                                       "max" : 100.0])
    }

    static var emergency_mockup: [Log] {
        return [Log(type: .emergency, created: Date(), data: ["value" : true]),
                Log(type: .emergency, created: Date(), data: ["value" : true]),
                Log(type: .emergency, created: Date(), data: ["value" : true])]
    }

    init(type: LogType, created: Date, data: [String : Any]) {
        self.type = type
        self.created = created
        self.data = data
    }

    convenience init?(json: JSON?) {
        guard let json = json
            , let type = json["class"].string
            , let logType = LogType(rawValue: type)
            , let created = json["created"].double
            , let data = json["data"].dictionary else {
            return nil
        }
        self.init(type: logType, created: Date(timeIntervalSince1970: created/1000), data: data)
    }

    convenience init?(emergencyJson: JSON?) {
        guard let json = emergencyJson
            , let created = json["date"].double else {
                return nil
        }
        self.init(type: .emergency, created: Date(timeIntervalSince1970: created/1000), data: [:])
    }

}
