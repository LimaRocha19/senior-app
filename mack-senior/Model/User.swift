//
//  User.swift
//  mack-senior
//
//  Created by Isaías Lima on 29/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit
import JASON

enum UserType: String {
    case family = "family"
    case care = "care"
}

class User {

    var email: String
    var name: String
    var type: UserType
    var telephone: String
    var _id: String

    var patients: [Patient]

    static var mockup: User {
        return User(email: "juvenaldo.carreira@macksenior.com.br", name: "Juvenaldo Carreira", type: .care, telephone: "+5540028922", _id: "1234567890")
    }

    init(email: String, name: String, type: UserType, telephone: String, _id: String) {
        self.email = email
        self.name = name
        self.type = type
        self.telephone = telephone
        self._id = _id
        self.patients = []
    }

    convenience init?(json: JSON?) {
        guard let json = json
            , let email = json["email"].string
            , let type = json["class"].string
            , let userType = UserType(rawValue: type)
            , let name = json["name"].string
            , let last = json["last"].string
            , let telephone = json["telephone"].string
            , let id = json["_id"].string else {
                return nil
        }
        self.init(email: email, name: name + " " + last, type: userType, telephone: telephone, _id: id)
    }

    func add(patient: Patient) {
        self.patients.append(patient)
    }

    func add(patients: [Patient]) {
        self.patients.append(contentsOf: patients)
    }

    func remove(_id: String) {
        self.patients = self.patients.filter({ $0._id != _id })
    }

    func erase() {
        self.patients = []
    }
}
