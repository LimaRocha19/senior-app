//
//  Patient.swift
//  mack-senior
//
//  Created by Isaías Lima on 29/09/18.
//  Copyright © 2018 Isaías. All rights reserved.
//

import UIKit
import JASON

class Patient {

    var name: String
    var birth: String
    var _id: String

    static var mockup: [Patient] {
        return [Patient(name: "Maria das Graças", birth: "11/09/1905", _id: "1234567890"),
                Patient(name: "José Pereira Garibaldi", birth: "30/10/1925", _id: "0987654321"),
                Patient(name: "Sandro Pereira Gomes", birth: "02/02/1902", _id: "6789054321"),
                Patient(name: "Marília Gusmão", birth: "01/12/1945", _id: "5432167890")]
    }

    init(name: String, birth: String, _id: String) {
        self.name = name
        self.birth = birth
        self._id = _id
    }

    convenience init?(json: JSON?) {
        guard let json = json
            , let name = json["name"].string
            , let birth = json["birth"].string
            , let _id = json["_id"].string else {
                return nil
        }
        self.init(name: name, birth: birth, _id: _id)
    }
}
