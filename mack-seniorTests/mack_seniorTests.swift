//
//  mack_seniorTests.swift
//  mack-seniorTests
//
//  Created by Isaías Lima on 16/09/2018.
//  Copyright © 2018 Isaías. All rights reserved.
//

import XCTest
@testable import mack_senior

enum Result<T> {
    case success(T)
    case error(Error)
}

class mack_seniorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSignup() {

        let params: [String : String] = ["email" : "\(arc4random() % 1000000)@test.com",
            "password" : "testIsAwesome\(arc4random() % 2000000)",
            "name" : "Testable \(arc4random() % 2000000)",
            "last" : "Testy \(arc4random() % 2000000)",
            "class" : "family",
            "telephone" : "+5540028922"]

        let expectation = self.expectation(description: "Signup")
        var error: Error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: nil)
        var user: User = User.mockup

        Service.signup(params: params) { (status) in
            switch status {
            case .success(let usr):
                user = usr
                expectation.fulfill()
            case .failure(let err):
                error = err
                expectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5, handler: nil)

        if user._id != User.mockup._id {
            XCTAssert(true, "Usuário teste criado com sucesso")
        } else {
            XCTAssert(false, error.localizedDescription)
        }
    }

    func testSigninCare() {

        let params: [String : String] = ["email" : "isaiahlima18@gmail.com", "password" : "st10900152"]

        let expectation = self.expectation(description: "Signin Care")
        var error: Error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: nil)
        var user: User = User.mockup

        Service.signin(params: params) { (status) in
            switch status {
            case .success(let usr):
                user = usr
                expectation.fulfill()
            case .failure(let err):
                error = err
                expectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5, handler: nil)

        if user._id != User.mockup._id {
            XCTAssert(true, "Usuário teste logado com sucesso")
        } else {
            XCTAssert(false, error.localizedDescription)
        }
    }

    func testForgot() {

        let expectation = self.expectation(description: "Forgot")
        var error: Error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: nil)
        var message: String = ""

        Service.reset_password(email: "g_bertho@gmail.com") { (status) in
            switch status {
            case .success(_):
                message = "completed"
                expectation.fulfill()
            case .failure(let err):
                error = err
                expectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5, handler: nil)

        if message != "" {
            XCTAssert(true, "Recuperação de senha solicitada")
        } else {
            XCTAssert(false, error.localizedDescription)
        }
    }

//    func testPatients() {
//
//        let expectation = self.expectation(description: "Patients")
//        var error: Error = NSError(domain: NSCocoaErrorDomain, code: 404, userInfo: nil)
//        var patients: [Patient] = []
//
//        Service.patients { (status) in
//            switch status {
//            case .success(let p):
//                patients = p
//                expectation.fulfill()
//            case .failure(let err):
//                error = err
//                expectation.fulfill()
//            }
//        }
//
//        waitForExpectations(timeout: 5, handler: nil)
//
//        if !patients.isEmpty {
//            XCTAssert(true, "Pacientes Recuperados")
//        } else {
//            XCTAssert(false, error.localizedDescription)
//        }
//    }
}
